name: Sanitizer

on:
  push:
    branches: [ main ]
    paths-ignore: [ 'doc/**', '**.md' ]
  pull_request:
    branches: [ main ]
    paths-ignore: [ 'doc/**', '**.md' ]
    types: [ assigned ]
  schedule:
    - cron: '0 12 * * 1-5'

jobs:
  # TODO(MBkkt) Add MEMSAN?
  # TODO(MBkkt) Maybe better merge undefined, address, leak?
  # TODO(MBkkt) Other not google sanitizers?

  main:
    runs-on: ${{ matrix.os }}
    strategy:
      fail-fast: false
      matrix:
        os: [ ubuntu-20.04, macos-11, windows-2022 ]
        flags: [ WARN, UBSAN, ASAN, TSAN, LSAN ]
        build_type: [ Debug, RelWithDebInfo ]
        isPR:
          - ${{ github.event_name == 'pull_request' }}
        exclude:
          - isPR: true
            os: macos-11
            flags: UBSAN
            build_type: Debug
          - isPR: true
            os: macos-11
            flags: UBSAN
            build_type: RelWithDebInfo
          - isPR: true
            os: macos-11
            flags: ASAN
            build_type: Debug
          - isPR: true
            os: macos-11
            flags: ASAN
            build_type: RelWithDebInfo
          - isPR: true
            os: macos-11
            flags: TSAN
            build_type: Debug
          - isPR: true
            os: macos-11
            flags: TSAN
            build_type: RelWithDebInfo
          - isPR: true
            os: macos-11
            flags: LSAN
            build_type: Debug
          - isPR: true
            os: macos-11
            flags: LSAN
            build_type: RelWithDebInfo
          #  Doesn't build with ClanCL ASAN. Random crash on start before main() with MSVC ASAN. THANKS TO THE BEST OS.
          - os: windows-2022
            flags: ASAN
            build_type: Debug
          - os: windows-2022
            flags: ASAN
            build_type: RelWithDebInfo
          # Best OS doesn't support unnecessary things like UBSAN, LSAN, TSAN.
          # May be on best OS you can't write a program with undefined behavior or memory leak or race condition, dunno.
          # I also have a conspiracy theory that Windows is just an unnecessary abstraction over Linux.
          # See how well WSL works. Or how quickly they made the analog of io_uring.
          # And who cares if the virtual machine falls? I think this explains everything.
          - os: windows-2022
            flags: UBSAN
            build_type: Debug
          - os: windows-2022
            flags: UBSAN
            build_type: RelWithDebInfo
          - os: windows-2022
            flags: LSAN
            build_type: Debug
          - os: windows-2022
            flags: LSAN
            build_type: RelWithDebInfo
          - os: windows-2022
            flags: TSAN
            build_type: Debug
          - os: windows-2022
            flags: TSAN
            build_type: RelWithDebInfo

    env:
      YACLIB_FAULT: 'OFF ON' # TODO(myannyax)

    steps:
      - uses: actions/checkout@v2

      - name: Update deps on Linux
        if: ${{ matrix.os == 'ubuntu-20.04' }}
        run: |
          sudo apt-get update
          sudo wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -
          sudo add-apt-repository "deb http://apt.llvm.org/focal/     llvm-toolchain-focal-14   main"
          sudo apt-get update
          sudo apt-get install ninja-build gcc-11 g++-11 clang-14 lld-14 libc++-14-dev libc++abi-14-dev googletest
          sudo ln -sf /usr/bin/lld-14 /usr/local/bin/ld

      - name: Update deps on macOS
        if: ${{ matrix.os == 'macos-11' }}
        run: |
          brew update
          brew install ninja llvm@13

      - name: Set up MinGW x64
        if: ${{ matrix.os == 'windows-2022' }}
        uses: egor-tensin/setup-mingw@v2
        with:
          platform: x64

      - name: Configure CMake Posix
        if: ${{ matrix.os != 'windows-2022' }}
        run: |
          flags=${{ matrix.flags }}
          build_type=${{ matrix.build_type }}
          compilers_name=(clang gnu apple_clang)
          stdlibs_name=(libcxx libstdcxx apple_libcxx)
          if [[ ${{ matrix.os }} == "macos-11" ]]; then
            llvm=/usr/local/opt/llvm
            c_compilers=("$llvm/bin/clang" gcc-11 clang)
            cxx_compilers=("$llvm/bin/clang++" g++-11 clang++)
            link_options=(
              "-L$llvm/lib;-Wl,-rpath,$llvm/lib"
              "-stdlib=libstdc++"
              "" # Apple
            )
            compile_options=(
              "-I$llvm/include;-I$llvm/include/c++/v1/"
              "-stdlib=libstdc++"
              "" # Apple
            )
            slowdown=8
          else
            c_compilers=(clang-14 gcc-11)
            cxx_compilers=(clang++-14 g++-11)
            link_options=(
              "-stdlib=libc++;-lc++abi"
              "-stdlib=libstdc++"
            )
            compile_options=(
              "-stdlib=libc++"
              "-stdlib=libstdc++"
            )
            slowdown=1
          fi

          for (( i=0; i<${#c_compilers[*]}; i+=1 )); do
            for (( j=0; j<${#link_options[*]}; j+=1 )); do
              for yaclib_fault in ${YACLIB_FAULT[*]}; do
                if [[ $yaclib_fault == ON ]]; then
                  continue  # TODO(myannyax)
                fi

                if [[ ${{ matrix.os }} == "macos-11" ]]; then # macOS workarounds
                  if [[ ${compilers_name[$i]} != "gnu" && $flags == "LSAN" ]]; then
                    # AppleClang doesn't support LSAN.
                    # But also random crash with LLVM CLang LSAN. TODO(myannyax) Try to debug and create issue to LLVM CLang LSAN.
                    # >CHECK failed: sanitizer_thread_registry.cpp:58 "((ThreadStatusFinished)) == ((status))" (0x3, 0x4) (tid=47505)
                    # Btw I don't think leak sanitizer is needed on a macOS.
                    # After all, if your application crashed from lack of memory, why fix it? Better buy a new mac!
                    continue
                  fi
                  if [[ ${compilers_name[$i]} == "clang" && ${stdlibs_name[$j]} == "libstdcxx" ]]; then
                    continue  # TODO(MBkkt) I think we should specify path to libstdcxx
                  fi
                  if [[ ${compilers_name[$i]} == "gnu" && ${stdlibs_name[$j]} == "libstdcxx" && $flags == "TSAN" ]]; then
                    continue  # TODO(MBkkt) Couldn't link against stdlib
                  fi
                fi

                link_option=""; compile_option=""
                if [[ ${compilers_name[$i]} == "apple_clang" ]]; then
                  if [[ ${stdlibs_name[$j]} != "apple_libcxx" ]]; then
                    continue  # TODO(MBkkt) I dunno how to get AppleClang to work with other stdlibs
                  fi
                elif [[ ${compilers_name[$i]} == "gnu" ]]; then
                  if [[ ${stdlibs_name[$j]} != "libstdcxx" ]]; then
                    continue  # TODO(MBkkt) I dunno how to get GNU GCC to work with other stdlibs
                    # Btw I dunno what stdlib would be using libstdcxx or apple_libcxx, just some default
                  fi
                else
                  link_option=${link_options[$j]}; compile_option=${compile_options[$j]}
                fi

                dir="build_${compilers_name[$i]}_${stdlibs_name[$j]}_fault_${yaclib_fault}"
                echo $dir

                cmake -S . -B $dir                                   \
                  -DCMAKE_BUILD_TYPE=$build_type                     \
                  -DYACLIB_CXX_STANDARD=20                           \
                  -DYACLIB_TEST=SINGLE                               \
                  -DYACLIB_FLAGS="CORO;$flags"                       \
                  -DCMAKE_C_COMPILER=${c_compilers[$i]}              \
                  -DCMAKE_CXX_COMPILER=${cxx_compilers[$i]}          \
                  -G"Ninja"                                          \
                  -DYACLIB_LINK_OPTIONS="$link_option"               \
                  -DYACLIB_COMPILE_OPTIONS="$compile_option"         \
                  -DYACLIB_DEFINITIONS="YACLIB_CI_SLOWDOWN=$slowdown"

              done
            done
          done

      - name: Configure CMake Windows
        if: ${{ matrix.os == 'windows-2022' }}
        shell: bash
        run: |
          flags=${{ matrix.flags }}

          names=(ClangCL MinGW MSVC)
          generators=("Visual Studio 17 2022" "MinGW Makefiles" "Visual Studio 17 2022")
          options=("-A x64 -T ClangCL" "-DCMAKE_BUILD_TYPE=${{ matrix.build_type }}" "-A x64")

          for (( i=0; i<${#names[*]}; i+=1 )); do
            for yaclib_fault in ${YACLIB_FAULT[*]}; do
              if [[ $yaclib_fault == "ON" ]]; then
                continue  # TODO(myannyax)
              fi

              if [[ $flags == ASAN && ${names[$i]} != MSVC ]]; then
                continue  # TODO(MBkkt) I can't build with any other ASAN on Windows
                # I think ClangCL with ASAN should be possible
              fi

              dir="build_${names[$i]}_fault_${yaclib_fault}"
              echo $dir

              cmake -S . -B $dir         \
                -DYACLIB_CXX_STANDARD=20 \
                -DYACLIB_TEST=SINGLE     \
                -DYACLIB_FLAGS=$flags    \
                -G"${generators[$i]}"    \
                ${options[$i]}

            done
          done

      - name: Build
        shell: bash
        run: |
          for dir in build*/; do
            echo $dir
            cmake --build $dir --config ${{ matrix.build_type }} --parallel
            echo
          done

      - name: Test
        shell: bash
        run: |
          flags=${{ matrix.flags }}
          default="help=0 verbosity=0 halt_on_error=0"
          export ASAN_OPTIONS="$default debug=1 detect_leaks=0 check_initialization_order=1 detect_stack_use_after_return=1 strict_init_order=1 strict_string_checks=1 detect_invalid_pointer_pairs=2"
          export TSAN_OPTIONS="$default history_size=0 io_sync=0 detect_deadlocks=1 second_deadlock_stack=1"  
          export UBSAN_OPTIONS="$default print_stacktrace=1"
          if [[ $flags == "LSAN" ]]; then  # This is necessary for the schizo macOS ASAN, who for some reason reads LSAN_OPTIONS
            export LSAN_OPTIONS="$default debug=1 detect_leaks=1"
          fi
          for dir in build*/; do
            cd $dir;
            ctest --output-on-failure -C ${{ matrix.build_type }} -V
            cd ..
          done
