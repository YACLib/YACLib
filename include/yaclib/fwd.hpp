#pragma once

namespace yaclib {

template <typename V, typename E>
class Result;

template <typename V, typename E>
class Future;

template <typename V, typename E>
class Promise;

}  // namespace yaclib
