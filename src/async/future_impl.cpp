#include <yaclib/async/future.hpp>

namespace yaclib {

template class Future<void, StopError>;

}  // namespace yaclib
